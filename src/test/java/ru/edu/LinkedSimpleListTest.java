package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LinkedSimpleListTest {

    public static final String FIRST = "First";
    public static final String SECOND = "Second";
    public static final String THIRD = "Third";
    public static final String FOURTH = "Fourth";
    private SimpleList<String> list;


    @Before
    public void setUp() throws Exception {
        list = new LinkedSimpleList<>();
        list.add(FIRST);
        list.add(SECOND);
        list.add(THIRD);
        list.add(FOURTH);
    }

    @Test
    public void testList() {
        assertEquals(4, list.size());
        assertEquals(FIRST, list.get(0));
        assertEquals(SECOND, list.get(1));
        assertEquals(THIRD, list.get(2));
        assertEquals(FOURTH, list.get(3));
        assertEquals(3, list.indexOf(FOURTH));

        list.remove(1);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testIOutBoundException() {
        list.set(4,FIRST);
        list.get(4);
        list.remove(4);
    }

}