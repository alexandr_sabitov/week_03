package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedSimpleQueueTest {
    public static final String FIRST = "First";
    public static final String SECOND = "Second";
    public static final String THIRD = "Third";
    public static final String FOURTH = "Fourth";
    private LinkedSimpleQueue<String> queue;

    @Before
    public void setUp() throws Exception {
        queue = new LinkedSimpleQueue<>(4);
        queue.offer(FIRST);
        queue.offer(SECOND);
        queue.offer(THIRD);
        queue.offer(FOURTH);
    }

    @Test
    public void testList() {
        assertEquals(FIRST,queue.poll()) ;
        assertEquals(SECOND, queue.peek());
        assertEquals(3, queue.size());
        queue.offer("4");
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testExceptionOffer() {
        queue.offer("5");
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testExceptionPoll() {
        queue.poll();
        queue.poll();
        queue.poll();
        queue.poll();
        queue.poll();
        queue.poll();
    }
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testExceptionPeek() {
        int count = queue.size();
        for (int i = 0; i < count; i++) {
            queue.poll();
        }
        queue.peek();
    }
}

