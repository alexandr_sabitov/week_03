package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ArraySimpleQueueTest {
    public static final String FIRST = "First";
    public static final String SECOND = "Second";
    public static final String THIRD = "Third";
    public static final String FOURTH = "Fourth";

    private ArraySimpleQueue<String> queue;

    @Before
    public void setUp() throws Exception {
        queue = new ArraySimpleQueue<>(5);
        queue.offer(FIRST);
        queue.offer(SECOND);
        queue.offer(THIRD);
    }

    @Test
    public void size() {
        assertEquals(3, queue.size());
    }

    @Test
    public void offer() {
        queue.offer(FOURTH);
        assertEquals(4, queue.size());
    }

    @Test
    public void peek() {
        assertEquals(FIRST, queue.peek());
    }

    @Test
    public void poll() {
        assertEquals(FIRST, queue.poll());
        assertEquals(SECOND, queue.poll());
    }

    @Test
    public void capacity() {
        assertEquals(5, queue.capacity());
    }

    @Test
    public void testException() {
        ArraySimpleQueue<Integer> queueEx = new ArraySimpleQueue<>(3);
        queueEx.offer(1);
        queueEx.offer(2);
        queueEx.offer(3);
        boolean a = false;
        try {
            queueEx.offer(4);
        } catch (ArrayIndexOutOfBoundsException e) {
            a = true;
        }
        assertTrue(a);

        queueEx.poll();
        queueEx.poll();
        queueEx.poll();
        boolean b = false;
        try {
            queueEx.poll();
        } catch (IllegalArgumentException e) {
            b = true;
        }
        assertTrue(b);

        boolean zeroSize = false;
        try {
            queueEx.peek();
        } catch (IllegalArgumentException e) {
            zeroSize = true;
        }
        assertTrue(zeroSize);
    }
}