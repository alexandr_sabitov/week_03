package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySimpleListTest {
    public static final String FIRST = "First";
    public static final String SECOND = "Second";
    public static final String THIRD = "Third";
    public static final String FOURTH = "Fourth";
    public static final String FOURTHTEST = "FourthTest";

    private ArraySimpleList<String> list;

    @Before
    public void setUp() throws Exception {
        list = new ArraySimpleList<>(5);
        list.add(FIRST);
        list.add(SECOND);
        list.add(THIRD);
        list.add(FOURTH);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testIOutBoundException() {
        list.set(4,FIRST);
        list.set(-1,FIRST);

    }

    @Test
    public void testList() {
        assertEquals(4, list.size());
        assertEquals(FIRST, list.get(0));
        assertEquals(SECOND, list.get(1));
        assertEquals(THIRD, list.get(2));
        assertEquals(FOURTH, list.get(3));
        assertEquals(3, list.indexOf(FOURTH));
        assertEquals(-1, list.indexOf("No"));
        list.set(3, FOURTHTEST);
        assertEquals(FOURTHTEST, list.get(3));

        list.remove(1);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
}