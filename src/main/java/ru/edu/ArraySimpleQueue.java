package ru.edu;


public class ArraySimpleQueue<T> implements SimpleQueue<T> {
    /**
     * Массив.
     */
    private T[] arr;

    /**
     * Колличество элементов очереди.
     */
    private int size;

    /**
     * Предельный размер очереди.
     */
    private int capacity;

    /**
     * Конструктор с параметром предельного размера.
     *
     * @param capacityT предельный размер очереди
     */
    public ArraySimpleQueue(final int capacityT) {
        this.capacity = capacityT;
        this.arr = (T[]) new Object[this.capacity];
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {
        if (size == capacity) {
            throw new ArrayIndexOutOfBoundsException("Очередь переполнена!");
        }
        if (size + 1 > arr.length) {
            return false;
        }
        arr[size] = value;
        size++;
        return true;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        if (size == 0) {
            throw new IllegalArgumentException("Очередь пуста!");
        }
        T value = arr[0];
        for (int i = 0; i < size - 1; i++) {

            arr[i] = arr[i + 1];

        }
        arr[size - 1] = null;
        size--;
        return value;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        if (size == 0) {
            throw new IllegalArgumentException("Очередь пуста. Метод peek() "
                    + "не может получить данные.");
        }
        return arr[0];
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return capacity;
    }
}
