package ru.edu;

public class LinkedSimpleQueue<T> implements SimpleQueue<T> {
    /**
     * Начало очереди.
     */
    private Node<T> head;

    /**
     * Конец очереди.
     */
    private Node<T> tail;

    /**
     * Колличество элементов в очереди.
     */
    private int size;

    /**
     * Педельный размер очереди.
     */
    private final int capacity;

    /**
     * Конструктор с параметром предельного размера.
     *
     * @param capacityTemp предельный размер очереди
     */
    public LinkedSimpleQueue(final int capacityTemp) {
        this.capacity = capacityTemp;
    }

    private static final class Node<T> {

        /**
         * Предыдущий элемент.
         */
        private Node prev;
        /**
         * Текущий элемент.
         */
        private T value;
        /**
         * Следующий элемент.
         */
        private Node next;

        private Node(final T val) {
            this.value = val;
        }
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {
        if (size == capacity) {
            throw new ArrayIndexOutOfBoundsException("Очередь переполнена!");
        }
        Node<T> node = new Node<>(value);
        if (size == 0) {
            head = node;
        } else {
            tail.next = node;
        }
        tail = node;
        size++;
        return true;

    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        if (size == 0) {
            throw new ArrayIndexOutOfBoundsException("Очередь пуста!");
        }
        T headValue = head.value;
        head = head.next;
        size--;
        if (size == 0) {
            tail = null;
        }
        return headValue;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        if (size == 0) {
            throw new ArrayIndexOutOfBoundsException("Очередь пуста!");
        }
        return head.value;
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return capacity;
    }
}
