package ru.edu;

public class LinkedSimpleList<T> implements SimpleList<T> {
    /**
     * Начало очереди.
     */
    private Node<T> head;

    /**
     * Конец очереди.
     */
    private Node<T> tail;

    /**
     * Кол-во элементов в массиве.
     */
    private int size;

    /**
     * Класс контейнер.
     * @param <T>
     */
    private static final class Node<T> {
        /**
         * Предыдущий элемент.
         */
        private Node prev;
        /**
         * Текущий элемент.
         */
        private T value;
        /**
         * Следующий элемент.
         */
        private Node next;

        private Node(final T val) {
            this.value = val;
        }
    }

    /**
     * Добавление элемента в конец списка.
     *
     * @param val элемент
     */
    @Override
    public void add(final T val) {
        Node<T> node = new Node<>(val);
        if (head == null) {
            head = node;
            tail = node;
        } else {
            node.prev = tail;
            tail.next = node;
            tail = node;
        }
        size++;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        correctIndex(index);
        findNode(index).value = value;
    }

    private void correctIndex(final int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException("Недопустимый индекс");
        }
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        correctIndex(index);
        Node<T> node = findNode(index);
        return node.value;
    }

    private Node<T> findNode(final int index) {
        Node<T> node = null;
        if (index < size / 2) {
            node = head;
            for (int i = 0; i < index; i++) {
                node = node.next;
            }

        } else {
            node = tail;
            for (int i = 0; i < size - index - 1; i++) {
                node = node.prev;
            }
        }
        return node;
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        correctIndex(index);
        if (size == 0) {
            System.out.println("Список пуст...");
        }
        if (size == 1) {
            head = null;
            tail = null;
            size = 0;
            return;
        }
        if (index == 0) {
            Node<T> next;
            next = head.next;
            head = next;
            size--;
            return;
        }
        if (index == (size - 1)) {
            Node<T> prev = tail.prev;
            prev.next = null;
            tail = prev;
            size--;

        } else {
            // [i-1] <prv[i]next> [i+1]
            // [i-1]next> [i+1]
            // [i-1] <prev [i+1]
            Node<T> remove = findNode(index);
            remove.prev.next = remove.next;
            remove.next.prev = remove.prev;
            size--;
        }

    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return индекс элемента или -1 если не найден
     */
    @Override
    public int indexOf(final T value) {
        Node<T> node = head;
        for (int i = 0; i < size; i++) {
            if (node.value.equals(value)) {
                return i;
            }
            node = node.next;
        }
        return -1;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }
}
